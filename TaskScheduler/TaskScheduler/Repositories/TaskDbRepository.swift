//
//  TaskRepository.swift
//  MyTasks
//
//  Created by Amir on 6/27/17.
//  Copyright © 2017 Amir Khorsandi. All rights reserved.
//

import CoreStore

class TaskDbRepository {
    
    public class func fetchAll() -> [TaskManageModel]{
    
        return StoreHelper.sharedInstance.dataStack.fetchAll(From(TaskManageModel.self)) ?? []
    }
    
    
    
    public class func new(name:String, date:Date, author:String, desc:String){
        
        _ = try? StoreHelper.sharedInstance.dataStack.perform(
            synchronous: { (transaction) in 
                
                let newTask = transaction.create(Into<TaskManageModel>())
                newTask.name = name
                newTask.author = author
                newTask.date = date
                newTask.desc = desc
          }
        )
        
    }

    
    public class func remove(task:TaskManageModel){
        
        _ = try? StoreHelper.sharedInstance.dataStack.perform(
            synchronous: { (transaction) in
                
                transaction.delete(task)
          }
        )
    }
    
    
    public class func edit(task:TaskManageModel){
        
        _ = try? StoreHelper.sharedInstance.dataStack.perform(
            synchronous: { (transaction) in
                
                transaction.edit(task)
        }
        )
    }

}
